# A Simple Parameter Inference with Transformers

Tento projekt sa zaoberá predikciou parametrov periodickej funkcie pomocou transfomeru. Cieľom je naučiť model odhadnúť amplitúdu ($A$) a uhlovú frekvenciu ($ω$) na základe diskrétnej reprezentácie periodickej funkcie, ktorá je definovaná vzťahom:

$y_i(A_i,ω_i,t)=A_isin⁡(ω_it)$

kde časová premenná $t$ je obmedzená na interval $t∈[0, 2π / ω_i]$.